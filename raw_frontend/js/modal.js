function openModal(instance) {
    $(instance).next().css("display", "block");
}

function closeModal(instance) {
    $(instance).parents().find(".modal").css("display", "none");
}