"""black_gray_white URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.urls import path, include
from django.conf import settings
from domain.views import export, IndexTemplateView, ImportDomainsView, SingleDomainCreateView, DeviceCreateView,\
    DeviceUpdateView, ListCreateView, ListUpdateView, DomainUpdateView
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('api.urls')),
    path('export/', export.as_view()),
    path('upload/', ImportDomainsView.as_view(), name="list_upload"),
    path('domain/create/', SingleDomainCreateView.as_view()),
    path('domain/update/', DomainUpdateView.as_view()),
    path('device/create/', DeviceCreateView.as_view(), name="device_create"),
    path('device/update/', DeviceUpdateView.as_view(), name="device_update"),
    path('list/create/', ListCreateView.as_view(), name="list_create"),
    path('list/update/', ListUpdateView.as_view(), name="list_update"),
    path('login/', LoginView.as_view(template_name='admin/login.html'), name="login"),
    path('', IndexTemplateView.as_view()),

]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns
