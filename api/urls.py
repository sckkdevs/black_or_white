from django.urls import path

from .views import DomainDetailApiView

urlpatterns= [

    path('domain', DomainDetailApiView.as_view(), name="domain_detail")
]