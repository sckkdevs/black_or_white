from django.shortcuts import render
from rest_framework.views import APIView
from django.http.response import JsonResponse
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
from urllib.parse import urlunsplit, urlsplit# Create your views here.
from django.core.exceptions import ValidationError

from domain.utils import get_domain_data
from domain.models import Domain

@method_decorator(staff_member_required, name='dispatch')
class DomainDetailApiView(APIView):
    def get(self, request):
        def split_url(url):
            try:
                return list(urlsplit(url))
            except ValueError:
                # urlparse.urlsplit can raise a ValueError with some
                # misformatted URLs.
                raise ValidationError("invalid url", code='invalid')
        domain = self.request.GET['domain']
        value = domain
        url_fields = split_url(value)
        if not url_fields[0]:
            url_fields[0] = 'http'
        if not url_fields[1]:
            url_fields[1] = url_fields[2]
            url_fields[2] = ''
            url_fields = split_url(urlunsplit(url_fields))
        value = urlunsplit(url_fields)

        # if not domain.startswith("http"):
        #     domain = "http://" + domain
        if not value.endswith('/'):
            value += '/'
        domain = Domain.objects.filter(domain_name=value).first()
        if domain:
            data = { "message": f"This domain already exists within \"{domain.list.name}\" which belongs to device \"{domain.list.device.name}\""}
            return JsonResponse(
                {"domain": value,
                 "exist": True,
                 "message": f"This domain already exists within \"{domain.list.name}\" which belongs to device \"{domain.list.device.name}\""},
                  status=404
            )
        else:
            data = get_domain_data(value)
        return JsonResponse({"domain": value, "data": data})
