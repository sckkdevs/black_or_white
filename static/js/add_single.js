function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

function addSingle(button) {
    console.log(button);

    const data = {
        domain_name: domain_data.name,
        category: $(button).hasClass("button-blacklist") ? 0 : 1,
        list: $("select.list").val()
    }

    $.post({
        url: "/domain/create/",
        data: data,
        dataType: "json",
        headers: {
            'X-CSRFToken': getCookie("csrftoken")
        }
    })
    .done((response) => {
        console.log(response);
    })
    .fail((error) => {
        console.log(error);
    })

}