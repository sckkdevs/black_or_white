function upload_file() {
    const input = $("#file")[0];
    if (input.files && input.files[0]) {
        const file = input.files[0];
        const reader = new FileReader();
        reader.onload = function() {
            console.log(reader.result);
        };
        reader.readAsText(file);
    } else {
        alert("Wystąpił błąd. Upewnij się że dodałeś plik.");
    }
}