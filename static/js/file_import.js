let list_name = "";

function addToWhiteList(button) {
    const row = $(button).parent().parent().parent();
    console.log(row);
    const status = row.find(".status");

    const data = {
        id: row.prop("id"),
        domain_name: row.find(".name").text(),
        category: 1,
        list: list_name
    }

    console.log(data);

    $.ajax({
        url: "/domain/update/",
        data: data,
        dataType: "json",
        method: "post",
        headers: {
            'X-CSRFToken': getCookie("csrftoken")
        }
    })
    .done((response) => {
        console.log(response);
        status.append(icon_ok);
    })
    .fail((error) => {
        console.log(error);
        status.append(icon_error);
    })

}


$("#file-upload").on("submit", (event) => {
    event.preventDefault();
    console.log(event);

    const data = new FormData(event.currentTarget);
    list_name = $($(event.currentTarget[3]).find(":selected")[0]).val();

    $.post({
        url: "/upload/",
        data: data,
        dataType: "json",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        headers: {
            'X-CSRFToken': getCookie("csrftoken")
        }
    })
    .done((response) => {
        console.log(response);

        $(".results-container").removeClass("before-uploading");
        $(".domain-table").removeClass("hide");

        const table = $(".domain-table");

        response.forEach((domain) => {
            if(domain.valid) {
                table.append(`
                    <tr id="${domain.id}">
                        <td class="name">${domain.domain_name}</td>
                        <td>${domain.data.summary.harmless}</td>
                        <td>${domain.data.summary.suspicious}</td>
                        <td>${domain.data.summary.malicious}</td>
                        <td>${domain.data.summary.undetected}</td>
                        <td>
                            <div class="results-buttons">
                                <button class="button-whitelist" onclick="addToWhiteList(this)">Biała lista</button>
                            </div>
                        </td>
                        <td class="status"></td>
                    </tr>
                `);
            }
        })
    })

})