from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin, ExportActionModelAdmin
from .models import Domain, Device, List
from .resources import create_resource
# Register your models here.


class DomainModelAdmin(ImportExportActionModelAdmin):
    # readonly_fields = ('sha256_hash', 'virustotal')
    fields = ('domain_name', 'list', 'category')
    resource_class = create_resource(Domain, ('domain_name', 'list__name'))


admin.site.register(Domain, DomainModelAdmin)
admin.site.register(Device)
admin.site.register(List)