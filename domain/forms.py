from django.forms import ModelForm, FileField, ModelChoiceField, CharField, Form

from .models import Domain, List, Device


class DomainForm(ModelForm):

    class Meta:
        model = Domain
        fields = ("domain_name", 'list', 'category')


class DomainListForm(Form):
    domain_list = FileField(label="Input File", help_text="List of domains with any delimiter")
    delimiter = CharField(max_length=5)
    list = ModelChoiceField(queryset=List.objects.all(), required=True)


class DeviceModelForm(ModelForm):

    class Meta:
        model = Device
        fields = ('name', )

class ListModelForm(ModelForm):

    class Meta:
        model = List
        fields = ("name", 'device')





