from django.db import models
from django.db.models.signals import pre_save
from django.db.models import Q
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.conf import settings
from django.core.validators import URLValidator
from  _sha256 import sha256
import requests
# Create your models here.


LIST_CHOICES = (
    (0, 'blacklist'),
    (1, 'whitelist'),
)

def domain_validator(value):
    qs = Domain.objects.filter(domain_name__iexact=value)
    if qs.exists():
        raise ValidationError(f"This domain already exists within \"{qs.first().list.name}\" which belongs to device \"{qs.first().list.device.name}\"")


class Device(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class List(models.Model):
    name                = models.CharField(max_length=100)
    short_description   = models.TextField(blank=True)
    device              = models.ForeignKey(Device, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Domain(models.Model):

    domain_name     = models.URLField()
    category        = models.SmallIntegerField(choices=LIST_CHOICES, default=0)
    list            = models.ForeignKey(List, on_delete=models.CASCADE)
    virustotal      = models.TextField(blank=True)
    sha256_hash     = models.CharField(max_length=64, unique=True, blank=True)

    def __str__(self):
        return self.domain_name

    def get_data(self):
        url = f"https://www.virustotal.com/ui/urls/{self.sha256_hash}?relationships=network_location%2Clast_serving_ip_address"
        data = requests.get(url).json()['data']['attributes']
        print(f"https://www.virustotal.com/ui/urls/{self.sha256_hash}/network_location")
        whois = requests.get(f"https://www.virustotal.com/ui/urls/{self.sha256_hash}/network_location").json()['data']['attributes'].get('whois', 'No data')
        engines = data['last_analysis_results']
        summary = data['last_analysis_stats']
        result = {**engines, 'whois': whois, "summary": {**summary}}
        return result

    # def get_data(self):
    #     url = "https://openpagerank.com/api/v1.0/getPageRank/"
    #     a = requests.get(url, headers={"API-OPR": settings.DOMAIN_API_KEY}, params={'domains[]': [self.domain_name]})
    #     return a.json()["response"]

    def get_whois(self):
        whois = requests.get(f"https://www.virustotal.com/ui/urls/{self.sha256_hash}/network_location").\
            json()['data']['attributes']['whois']

    def clean(self):
        if self.pk:
            qs = Domain.objects.filter(Q(domain_name=self.domain_name) & ~Q(id=self.id))
        else:
            qs = Domain.objects.filter(domain_name__iexact=self.domain_name)
        if qs.exists():
            raise ValidationError(
                f"This domain already exists within \"{qs.first().list.name}\" which belongs to device "
                f"\"{qs.first().list.device.name}\"")


@receiver(pre_save, sender=Domain)
def generate_sha256(sender, instance, **kwargs):
    if  not instance.domain_name.endswith('/'):
        instance.domain_name += '/'
    instance.sha256_hash = sha256(bytes(instance.domain_name, 'utf-8')).hexdigest()
    instance.virustotal = instance.get_data()