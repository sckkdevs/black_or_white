
from import_export import resources

def create_resource(django_model, model_fields):
    class model_resource(resources.ModelResource):
        class Meta:
           model = django_model
           fields = model_fields
    return model_resource