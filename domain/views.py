from django.shortcuts import render
from django.views.generic import View, TemplateView, CreateView, UpdateView, DeleteView
from django.http import HttpResponse, JsonResponse, Http404
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
from .models import Domain, Device, List
from .forms import DomainListForm, DomainForm, DeviceModelForm, ListModelForm
from .resources import create_resource
from .utils import get_domain_data
# Create your views here.

IMPORT_FORMATS = ('csv', 'json')



@method_decorator(staff_member_required, name='dispatch')
class IndexTemplateView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexTemplateView, self).get_context_data(**kwargs)
        # context['lists'] = List.objects.all()
        context['devices'] = Device.objects.all()
        return context


class export(View):
    def get(self, request):
        qs = Domain.objects.all()
        resource = create_resource(Domain, ['domain_name', 'list'])
        dataset = resource.export(qs)
        return HttpResponse(dataset.json)

@method_decorator(staff_member_required, name='dispatch')
class ImportDomainsView(TemplateView):
    template_name = 'file_upload.html'

    def get_context_data(self, **kwargs):
        context = super(ImportDomainsView, self).get_context_data(**kwargs)
        context['devices'] = Device.objects.all()
        context['form'] = DomainListForm()
        context['single_form'] = DomainForm()
        return context

    def post(self, request):
        output_domains = []
        upFile = request.FILES['domain_list']
        domains = upFile.read().decode('utf-8')
        if domains[-1] == '\n':
            domains=domains[:-1]
        delimiter = request.POST['delimiter']
        list_id = request.POST['list']
        form = DomainListForm(request.POST, request.FILES)
        if not form.is_valid():
            return render(request, 'file_upload.html', {"form":form})
        for domain in domains.split(delimiter):
            if not domain.endswith('/'):
                domain += '/'
            dataset = {"domain_name": domain, "list": list_id, "category": 0}
            form = DomainForm(dataset)
            out = {}
            if form.is_valid():
                object = form.save()
                out["valid"] = True
                out["id"] = object.id
                out["domain_name"] = form.cleaned_data['domain_name']
                out['data'] = get_domain_data(form.cleaned_data['domain_name'])
                output_domains.append(out)

            else:
                print(dataset['domain_name'])
                out["valid"] = False
                out['domain_name'] = dataset['domain_name']
                out['errors'] = form.errors.as_json()
                output_domains.append(out)
                print(form.errors.as_json())
        return JsonResponse(output_domains, safe=False)

@method_decorator(staff_member_required, name='dispatch')
class SingleDomainCreateView(CreateView):
    form_class = DomainForm
    model = Domain
    success_url = "/"
    def get(self,request):
        raise Http404

    def form_valid(self, form):
        domain = form.cleaned_data
        if not domain["domain_name"].endswith("/"):
            domain["domain_name"] += '/'
        if Domain.objects.filter(domain_name=form.cleaned_data['domain_name']).exists():
            return JsonResponse({"success": False, "message": f"This domain already exists within \"{domain['list'].name}\" which belongs to device \"{domain['list'].device.name}\""})
        a = form.save()
        return  JsonResponse({"success":True, "domain_name":a.domain_name, 'category': a.category, "list":{"id":a.list_id, "name":a.list.name}})
    def form_invalid(self, form):
        return JsonResponse(form.errors)


@method_decorator(staff_member_required, name='dispatch')
class DomainUpdateView(UpdateView):
    form_class = DomainForm
    model = Domain
    template_name = 'base.html'

    def get_object(self, queryset=None):
        return get_object_or_404(Domain, id=self.request.POST.get('id'))

    def get(self, request):
        raise Http404

    def form_valid(self, form):
        form.save()
        return JsonResponse({"success":True})
    def form_invalid(self, form):
        git return JsonResponse({"success": False, "errors": form.errors.as_json()})

@method_decorator(staff_member_required, name='dispatch')
class DeviceCreateView(CreateView):
    template_name = 'device_createform.html'
    form_class = DeviceModelForm
    success_url = "/"

    def form_valid(self, form):
        device = form.save()
        return JsonResponse({"succes": True, "name":device.name, "id": device.id})

    def form_invalid(self, form):
        return JsonResponse({"success": False, "message": form.errors.as_json})


@method_decorator(staff_member_required, name='dispatch')
class DeviceUpdateView(UpdateView):
    template_name = 'device_createform.html'
    form_class = DeviceModelForm
    success_url = '/'
    def form_valid(self, form):
        device = form.save()
        return JsonResponse({"succes": True, "name": device.name, "id": device.id})

    def form_invalid(self, form):
        return JsonResponse({"success": False, "message": form.errors.as_json})

    def get_object(self, queryset=None):
        id = self.request.GET.get('id')
        return get_object_or_404(Device, id=id)



@method_decorator(staff_member_required, name='dispatch')
class ListCreateView(CreateView):
    form_class = ListModelForm
    template_name = 'list_form.html'

    def form_valid(self, form):
        object = form.save()
        return JsonResponse({"success": True, "id":object.id, "name": object.name})

    def form_invalid(self, form):
        return JsonResponse({"success": False, "errors":form.errors.as_json()})


@method_decorator(staff_member_required, name='dispatch')
class ListUpdateView(UpdateView):
    form_class = ListModelForm
    template_name = 'list_form.html'


    def get_object(self, queryset=None):
        return  get_object_or_404(List, id=self.request.POST.get("id"))

    def form_valid(self, form):
        object = form.save()
        return JsonResponse({"success": True, "id": object.id, "name": object.name})

    def form_invalid(self, form):
        return JsonResponse({"success": False, "errors": form.errors.as_json()})