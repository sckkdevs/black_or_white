# Generated by Django 2.1.3 on 2018-11-24 20:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('domain', '0003_domain_list'),
    ]

    operations = [
        migrations.AlterField(
            model_name='domain',
            name='sha256_hash',
            field=models.CharField(blank=True, max_length=64, unique=True),
        ),
    ]
