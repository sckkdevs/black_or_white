import requests
import os
from django.conf import settings
from django.forms import ModelForm
from .models import Domain, List
from _sha256 import sha256

def get_domain_data(domain_name):
    print(domain_name)
    sha256_hash = sha256(bytes(domain_name, 'utf-8')).hexdigest()
    url = f"https://www.virustotal.com/ui/urls/{sha256_hash}?relationships=network_location%2Clast_serving_ip_address"
    print(url)
    data = requests.get(url).json()['data']['attributes']
    whois = requests.get(f"https://www.virustotal.com/ui/urls/{sha256_hash}/network_location").json()['data']['attributes']['whois']
    engines = data['last_analysis_results']
    summary = data['last_analysis_stats']
    result = {**engines, 'whois': whois, "summary": {**summary}}
    print(result)
    return result

# def get_domains_data(domain_list):
#     url = "https://openpagerank.com/api/v1.0/getPageRank/"
#     a = requests.get(url, headers={"API-OPR": settings.DOMAIN_API_KEY}, params={'domains[]':domain_list})
#     return a.json()["response"]

# class DomainForm(ModelForm):
#
#     class Meta:
#         model = Domain
#         fields = ("domain_name", 'list')
#
# file = open('../list.txt', 'r')
# delimiter = ";"
# domains = file.read()
# list_id = 1
# list = List.objects.get(id=list_id)
# for domain in domains.split(delimiter):
#     dataset = {"domain_name":domain, "list": list}
#     form = DomainForm(dataset)
#     print(form.is_valid())
